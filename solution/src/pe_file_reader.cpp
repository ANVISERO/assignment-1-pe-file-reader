/// @file
/// @brief File with realization of readPEHeaders()

#include "../include/pe_file_reader.h"

#include <stdbool.h>
#include <stdlib.h>

/// @brief Reads main header
/// @param[in] in PE File that need to read (input file)
/// @param[in] peFile pointer to PE file structure
/// @return True if main header had been read properly, false if something went wrong (error)
static bool read_header (FILE *in, struct PEFile *peFile) {
    return (fseek(in, SIGNATURE_OFFSET, SEEK_SET) == 0 &&
           fread(&peFile->header_offset, sizeof(peFile->header_offset), 1, in) != 0 &&
           fseek(in, peFile->header_offset, SEEK_SET) == 0 &&
           fread(&peFile->magic, sizeof (peFile->magic), 1, in) != 0 &&
           fread(&peFile->header, sizeof (peFile->header), 1, in) != 0);
}

/// @brief Reads optional header
/// @param[in] in PE File that need to read (input file)
/// @param[in] peFile pointer to PE file structure
/// @return True if optional header had been read properly, false if something went wrong (error)
static bool read_optional_header(FILE *in, struct PEFile *peFile) {
    return fseek(in, peFile->optional_header_offset, SEEK_SET) == 0;
}

/// @brief Reads section header
/// @param[in] in PE File that need to read (input file)
/// @param[in] peFile pointer to PE file structure
/// @return True if section header had been read properly, false if something went wrong (error)
static bool read_section_header(FILE * in, struct PEFile* peFile, size_t num) {
    size_t size_of_sections = sizeof(struct SectionHeader) * num;
    peFile->section_headers = malloc(size_of_sections);
    if (peFile->section_headers == NULL) {
        return false;
    }
    if (fseek(in, peFile->section_header_offset, SEEK_SET)) {
        return false;
    }
    if (fread(peFile->section_headers, size_of_sections, 1, in) != 1) {
        return false;
    }
    return true;
}

/// @brief Reads PE headers
/// @param[in] in PE File that need to read (input file)
/// @param[in] peFile pointer to PE file structure
/// @return The status of read (enum)
enum readStatus readPEHeaders (FILE *in, struct PEFile *peFile) {
    if (!read_header(in, peFile)) {
        fprintf(stderr, "Error occurred while reading the main header!\n");
        return FILE_READ_ERROR;
    }
    if (peFile->magic != SIGNATURE) {
        fprintf(stderr, "Invalid signature!\n");
        return INVALID_SIGNATURE;
    }
    peFile->magic_offset = SIGNATURE_OFFSET;
    peFile->optional_header_offset = peFile->header_offset + sizeof(peFile->magic) + sizeof(struct PEHeader);
    if (!read_optional_header(in, peFile)) {
        fprintf(stderr, "Error occurred while reading the optional header!\n");
        return FILE_READ_ERROR;
    }
    peFile->section_header_offset = peFile->optional_header_offset + peFile->header.SizeOfOptionalHeader;
    if (!read_section_header(in, peFile, peFile->header.NumberOfSections)) {
        if (peFile->section_headers == NULL) {
            fprintf(stderr, "Error occurred while allocating the memory!\n");
            free(peFile->section_headers);
            return MEMORY_ALLOCATION_ERROR;
        }
        free(peFile->section_headers);
        fprintf(stderr, "Error occurred while reading the section header!\n");
        return FILE_READ_ERROR;
    }
    return READ_OK;
}
