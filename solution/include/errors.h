#ifndef ERROR_H
#define ERROR_H
/// @file
/// @brief Program error status

/// Error status
enum error_status {
    /// Success
    OK = 0,
    /// Wrong number of arguments
    WRONG_NUMBER_OF_ARGS = 11,
    /// Can't find input file
    WRONG_INPUT_FILE_PATH = 22,
    /// Can't close the input file
    CLOSE_INPUT_FILE_ERROR = 33,
    /// Can't find output file
    WRONG_OUTPUT_FILE_PATH = 44,
    /// Can't close the output file
    CLOSE_OUTPUT_FILE_ERROR = 55,
    /// Error while reading headers
    READ_HEADERS_ERROR = 66,
    /// Error while extracting and writing the section of PE file
    WRITE_SECTION_OF_FILE_ERROR = 77
};

#endif
