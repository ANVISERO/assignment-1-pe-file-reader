#ifndef PE_SECTION_WRITER_H
#define PE_SECTION_WRITER_H
/// @file
/// @brief PE section writer

#include "pe_format.h"

#include "stdio.h"

/// Write PE section status
enum writeStatus {
    /// Write PE file section success
    WRITE_OK = 0,
    /// Write PE file section error
    WRITE_SECTION_ERROR = 1111,
    /// Such section was not found
    SECTION_NOT_FOUND = 2222,
    /// Can't read such a section
    READ_SECTION_ERROR = 3333,
    /// Not enough memory
    NOT_ENOUGH_MEMORY = 4444
};

/// @brief Writes a section of PE file in output file
/// @param[in] in PE File that need to read (input file)
/// @param[in] out File where we will write a section (output file)
/// @param[in] peFile Pointer to PE file structure
/// @param[in] name_of_section The name of the section of PE file
/// @return The status of write PE section (enum)
enum writeStatus writePESection (FILE *in, FILE *out, struct PEFile *peFile, char *name_of_section);

#endif
