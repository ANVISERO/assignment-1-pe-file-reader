#ifndef PE_FORMAT_H
#define PE_FORMAT_H
/// @brief This file describes the structure of PE file
/// @file

#include <stdint.h>

/// The file offset to the PE signature
#define SIGNATURE_OFFSET 0x3c
/// Signature ('PE\0\0')
#define SIGNATURE 0x4550

/// Structure describes standard COFF file header
struct
#ifdef _MSC_VER
    #pragma packed()
#else
    __attribute__((packed))
#endif
PEHeader {
    /// The number that identifies the type of target machine
    uint16_t Machine;
    /// The number of sections. This indicates the size of the section table, which immediately follows the headers
    uint16_t NumberOfSections;
    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970, which indicates when the file was created
    uint32_t TimeDateStamp;
    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present
    uint32_t PointerToSymbolTable;
    /// The number of entries in the symbol table
    uint32_t NumberOfSymbols;
    /// The size of the optional header, which is required for executable files but not for object files
    uint16_t SizeOfOptionalHeader;
    /// The flags that indicate the attributes of the file
    uint16_t Characteristics;
};

/// Structure describes general information that is useful for loading and running an executable file
struct
#ifdef _MSC_VER
    #pragma packed()
#else
    __attribute__((packed))
#endif
OptionalHeader {
    /// The unsigned integer that identifies the state of the image file
    uint16_t Magic;
    /// The linker major version number
    uint8_t MajorLinkerVersion;
    /// The linker minor version number
    uint8_t MinorLinkerVersion;
    /// The size of the code (text) section, or the sum of all code sections if there are multiple sections
    uint32_t SizeOfCode;
    /// The size of the initialized data section, or the sum of all such sections if there are multiple data sections
    uint32_t SizeOfInitializedData;
    /// The size of the uninitialized data section (BSS), or the sum of all such sections if there are multiple BSS sections
    uint32_t SizeOfUninitializedData;
    /// The address of the entry point relative to the image base when the executable file is loaded into memory
    uint32_t AddressOfEntryPoint;
    /// The address that is relative to the image base of the beginning-of-code section when it is loaded into memory
    uint32_t BaseOfCode;
};

/// Structure describes section headers data
struct
#ifdef _MSC_VER
#pragma packed()
#else
        __attribute__((packed))
#endif
SectionHeader {
    /// An 8-byte, null-padded UTF-8 encoded string
    char Name[8];
    /// The total size of the section when loaded into memory
    uint32_t VirtualSize;
    /// For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory
    uint32_t VirtualAddress;
    /// The size of the section (for object files) or the size of the initialized data on disk (for image files)
    uint32_t SizeOfRawData;
    /// The file pointer to the first page of the section within the COFF file. For executable images, this must be a multiple of FileAlignment from the optional header
    uint32_t PointerToRawData;
    /// The file pointer to the beginning of relocation entries for the section
    uint32_t PointerToRelocations;
    /// The file pointer to the beginning of line-number entries for the section
    uint32_t PointerToLinenumbers;
    /// The number of relocation entries for the section
    uint16_t NumberOfRelocations;
    /// The number of line-number entries for the section
    uint16_t NumberOfLinenumbers;
    /// The flags that describe the characteristics of the section
    uint32_t Characteristics;

};

/// Structure containing PE file data
struct
#ifdef _MSC_VER
    #pragma packed()
#else
        __attribute__((packed))
#endif
PEFile {
    /// @name Offsets within file
    ///@{

    /// Offset to a file magic
    uint32_t magic_offset;
    /// Offset to a main PE header
    uint32_t header_offset;
    /// Offset to an optional header
    uint32_t optional_header_offset;
    /// Offset to a section table
    uint32_t section_header_offset;
    ///@}

    /// @name File headers
    ///@{

    /// File magic
    uint32_t magic;
    /// Main header
    struct PEHeader header;
    /// Optional header
    struct OptionalHeader optional_header;
    /// Array of section headers with the size of header.number_of_sections
    struct SectionHeader *section_headers;
    ///@}
};

#endif
