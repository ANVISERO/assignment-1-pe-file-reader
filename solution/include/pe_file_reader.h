#ifndef PE_FILE_READER_H
#define PE_FILE_READER_H
/// @file
/// @brief PE header reader

#include "pe_format.h"

#include "stdio.h"

/// Read PE file status
enum readStatus {
    /// Read file success
    READ_OK = 0,
    /// Read file error
    FILE_READ_ERROR = 111,
    /// File has got invalid signature
    INVALID_SIGNATURE = 222,
    /// Error while memory allocation
    MEMORY_ALLOCATION_ERROR = 333
};

/// @brief Reads PE headers
/// @param[in] in PE File that need to read (input file)
/// @param[in] peFile pointer to PE file structure
/// @return The status of read (enum)
enum readStatus readPEHeaders (FILE *in, struct PEFile *peFile);

#endif
