/// @file
/// @brief File with realization of writePESection()

#include "../include/pe_section_writer.h"
#include <stdlib.h>
#include <string.h>


/// @brief Looks for the header of the section
/// @param[in] name  Pointer to name of the section
/// @param[in] peFile Pointer to PE file structure
/// @return The section header by the given name. (NULL if the section wasn't found)
static struct SectionHeader* find_section(char *name, struct PEFile *peFile) {
    for (size_t i = 0; i < peFile->header.NumberOfSections; i++) {
        if (!strcmp(peFile->section_headers[i].Name, name)) {
            return &peFile->section_headers[i];
        }
    }
    return NULL;
}

/// @brief Writes a section of PE file in output file
/// @param[in] in PE File that need to read (input file)
/// @param[in] out file where we will write a section (output file)
/// @param[in] peFile pointer to PE file structure
/// @param[in] name_of_section The name of the section of PE file
/// @return The status of write PE section (enum)
enum writeStatus writePESection(FILE *in, FILE *out, struct PEFile *peFile, char *name_of_section) {
    struct SectionHeader* current_section_header = find_section(name_of_section, peFile);
    if (current_section_header == NULL) {
        fprintf(stderr, "Section was not found!\n");
        return SECTION_NOT_FOUND;
    }
    char* section_data = malloc(current_section_header->SizeOfRawData);
    if (section_data == NULL) {
        fprintf(stderr, "Error occurred while allocating memory for section!\n");
        free(section_data);
        return NOT_ENOUGH_MEMORY;
    }
    if (fseek(in, current_section_header->SizeOfRawData, SEEK_SET)) {
        fprintf(stderr, "Error occurred while reading section!\n");
        free(section_data);
        return READ_SECTION_ERROR;
    }
    if (fread(section_data, current_section_header->SizeOfRawData, 1, in) != 1) {
        fprintf(stderr, "Error occurred while reading section!\n");
        free(section_data);
        return READ_SECTION_ERROR;
    }
    if (fwrite(section_data, current_section_header->SizeOfRawData, 1, out) != 1) {
        fprintf(stderr, "Error occurred while writing section!\n");
        free(section_data);
        return WRITE_SECTION_ERROR;
    }
    free(section_data);
    return WRITE_OK;
}
