/// @file 
/// @brief Main application file
#include "../include/errors.h"
#include "../include/pe_file_reader.h"
#include "../include/pe_section_writer.h"
#include <stdlib.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f) {
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc != 4) {
        usage(stdout);
        return WRONG_NUMBER_OF_ARGS;
    }
    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        fprintf(stderr, "Can't find input file\n");
        if (fclose(in)) {
            fprintf(stderr, "Can't close the input file!\n");
            return CLOSE_INPUT_FILE_ERROR;
        }
        return WRONG_INPUT_FILE_PATH;
    }
    struct PEFile peFile;
    if (readPEHeaders(in, &peFile) != READ_OK) {
        if (fclose(in)) {
            fprintf(stderr, "Can't close the input file!\n");
            return CLOSE_INPUT_FILE_ERROR;
        }
        return READ_HEADERS_ERROR;
    }
    FILE* out = fopen(argv[3], "wb");
    if (out == NULL) {
        fprintf(stderr, "Can't find output file\n");
        free(peFile.section_headers);
        if (fclose(out)) {
            fprintf(stderr, "Can't close the output file!\n");
            return CLOSE_OUTPUT_FILE_ERROR;
        }
        return WRONG_OUTPUT_FILE_PATH;
    }
    if (writePESection(in, out, &peFile, argv[2]) != WRITE_OK) {
        free(peFile.section_headers);
        if (fclose(in)) {
            fprintf(stderr, "Can't close the input file!\n");
            return CLOSE_INPUT_FILE_ERROR;
        }
        if (fclose(out)) {
            fprintf(stderr, "Can't close the output file!\n");
            return CLOSE_OUTPUT_FILE_ERROR;
        }
        return WRITE_SECTION_OF_FILE_ERROR;
    }
    free(peFile.section_headers);
    if (fclose(in)) {
        fprintf(stderr, "Can't close the input file!\n");
        return CLOSE_INPUT_FILE_ERROR;
    }
    if (fclose(out)) {
        fprintf(stderr, "Can't close the output file!\n");
        return CLOSE_OUTPUT_FILE_ERROR;
    }
    return OK;
}
